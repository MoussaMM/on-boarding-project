"""Add one dummy test to make the pipeline pass"""
from core.dummy import is_dump


def test_is_dump():
    assert is_dump() == 3
