# Contains all dependencies which are needed in the development environment

-r requirements-linters.txt
-r requirements-runtime.txt
-r requirements-tests.txt
